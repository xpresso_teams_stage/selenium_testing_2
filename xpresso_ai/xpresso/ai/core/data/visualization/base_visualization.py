"""Class for visualization of attribute metrics using seaborn, matplotlib"""

__all__ = ["BaseVisualization"]
__author__ = ["Ashritha Goramane"]

import pandas as pd

from xpresso.ai.core.commons.exceptions.xpr_exceptions import \
    MultivariatePlotException
from xpresso.ai.core.commons.utils.constants import REPORT_OUTPUT_PATH, \
    EMPTY_STRING
from xpresso.ai.core.data.automl.data_type import DataType
from xpresso.ai.core.data.visualization import utils
from xpresso.ai.core.data.visualization.abstract_visualization import \
    PlotType, \
    AbstractVisualize
from xpresso.ai.core.data.visualization.report import UnivariateReport, \
    MultivariateReport, CombinedReport, ReportParam


class BaseVisualization(AbstractVisualize):
    """
        Visualization class takes a automl object and provide functions
        to render plots on the metrics of attribute
        Args:
            dataset(:obj StructuredDataset): Structured dataset object on
            which visualization to be performed
    """

    def __init__(self, dataset):
        super().__init__(dataset)

    def process_input(self, field=None, attr=None, plot_type=None):
        """
        Helper function to process input fields for a plot generation
        Args:
            field: The metric field for plotting
            attr: The attribute whose plot to be generated
            plot_type: The type of plot to be generated

        Returns:
            (str) plot_file_name: file name of the plot to be saved,
            (str) plot_title: title for the plot to be generated,
            (list) input_1: input for x axis, (list) input_2: input for y
            axis, (dict) axes_labels: labels for x and y axis
        """
        if field in attr.metrics.keys():
            self.logger.info("{} plot for Attribute : ({},{},{})".format(
                plot_type, attr.name, attr.type, field))
        else:
            self.logger.error(
                "Attribute : {}, Field : {}. Unable to perform visualization"
                "Key not present in metric".format(attr.name, field))
            return False
        plot_file_name = "{}_{}".format(attr.name, plot_type)
        plot_title = plot_type + ' plot for ' + attr.name
        plot_title = plot_title.capitalize()
        input_1 = list()
        input_2 = list()
        if field == "quartiles":
            axes_labels = self.set_axes_labels(EMPTY_STRING, EMPTY_STRING)
            input_1 = list(attr.metrics[field])
        elif field in ["freq_count", "day_count", "month_count", "year_count"]:
            axes_labels = self.set_axes_labels(str(attr.name), "Count")
            input_1 = list(attr.metrics[field].keys())
            input_2 = list(attr.metrics[field].values())
        elif field == "pdf":
            axes_labels = self.set_axes_labels(str(attr.name),
                                               "Count({})".format(attr.name))
            input_1 = list(attr.metrics[field].keys())
            input_2 = list(attr.metrics[field].values())
        else:
            return False
        return plot_file_name, plot_title, input_1, input_2, axes_labels

    def target_variable_plot(self, target, attr_name=None,
                             output_format=utils.HTML):
        """

        Helper function to plot all combination of target variable plots
        Args:
            target(:str): Target variable name
            attr_name(:str): optional attribute name to plot specific
            attribute vs target plots
            output_format (str): html, png or pdf plots to be generated
        """

    def plot(self, field=None, attr=None, plot_type=None,
             output_format=utils.HTML, others_percent_threshold=None,
             bar_plot_tail_threshold=None):
        """
        Renders specific plot for field in metrics depending upon the type
                of plot
        field(str): Metric field of the data to be plotted
        attr(:obj:AttributeInfo): Attribute of which plot to be generated
        plot_type (:obj: `PlotType`): Specifies the type of plot
            to be generated
        output_format (str): html, png or pdf plots to be generated
        others_percent_threshold(int): Optional threshold to club small
            categories in pie chart
        bar_plot_tail_threshold(int): Optional threshold to club tail
            in bar chart
        """

    def render_univariate(self, attr_name=None, attr_type=None, plot_type=None,
                          output_format=utils.HTML,
                          output_path=REPORT_OUTPUT_PATH, report=False,
                          report_format=ReportParam.SINGLEPAGE.value,
                          file_name=None, target=None,
                          others_percent_threshold=None,
                          bar_plot_tail_threshold=None):
        """
        Renders univariate graphs for a particular attribute
        Args:
            attr_name (str): Specific attribute for which plotutils to be
                generated
            attr_type (:obj: `DataType`): Specific type whose plot_type has
            to be changed
            plot_type (:obj: `PlotType`): Specifies the type of plot to be
                generated
            output_format (str): html, png or pdf plots to be generated
            output_path(str): path where the html plots to be stored
            report (bool): if true generates a report
            report_format (:obj: `ReportParam`): whether the attribute
            distribution should be on same page or continued on different pages
            file_name(str): file name of report pdf
            target(str): Name of target variable in dataset
            others_percent_threshold(int): Optional threshold to club small
                categories in pie chart
            bar_plot_tail_threshold(int): Optional threshold to club tail
                in bar chart
        """
        if report_format.lower() not in [ReportParam.SINGLEPAGE.value,
                                         ReportParam.DOUBLEPAGE.value]:
            print("Incorrect report_format provided. Acceptable parameter "
                  "values are [single,double]")

        if report:
            output_format = "png"

        if target:
            self.target_variable_plot(target=target, attr_name=attr_name,
                                      output_format=output_format)
        self.univariate_plot(attr_name, attr_type, plot_type, output_format,
                             others_percent_threshold=others_percent_threshold,
                             bar_plot_tail_threshold=bar_plot_tail_threshold)
        if report:
            UnivariateReport(dataset=self.dataset).create_report(
                output_path=output_path,
                report_format=report_format,
                file_name=file_name)

    def render_multivariate(self, output_format=utils.HTML,
                            output_path=REPORT_OUTPUT_PATH,
                            report=False, file_name=None):
        """
        Renders multivariate graphs for spearman, pearson, chi-square
        correlation coefficient
        Args:
            output_format (str): html, png or pdf plots to be generated
            output_path(str): path where the html plots to be stored
            report (bool): if true generates a report
            file_name(str): file name of report pdf
        """
        if report:
            output_format = utils.PNG

        correlation = ["pearson", "spearman", "chi_square"]
        is_plotted = False
        for corr in correlation:
            if corr not in self.metric.keys():
                self.logger.info(
                    "Unable to do Multivariate Visualisation for  Field : {}. "
                    "Key not present in metric".format(corr))
                continue
            try:
                self.logger.info("{} plot for Correlation Type : {}".format(
                    PlotType.HEATMAP.value, corr))
                corr_df = pd.Series(list(self.metric[corr].values()),
                                    index=pd.MultiIndex.from_tuples(
                                        self.metric[corr].keys()))

                corr_df = corr_df.unstack()
                is_plotted = self.plot_multivariate(corr_df, corr,
                                                    output_format=output_format)

            except MultivariatePlotException as e:
                self.logger.error("{}".format(e.message))

        if not is_plotted:
            self.logger.error("No key found. Unable to plot for multivariate "
                              "metrics")

        if report:
            MultivariateReport(self.dataset).create_report(
                output_path=output_path,
                file_name=file_name)

    def plot_multivariate(self, corr_df, corr, output_format=utils.HTML):
        """Helper function to plot multivariate plots"""
        return False

    def render_all(self, output_format=utils.HTML,
                   output_path=REPORT_OUTPUT_PATH, report=False,
                   report_format=ReportParam.SINGLEPAGE.value, file_name=None,
                   target=None):
        """
        Renders all univariate and multivariate graphs
        Args:
            output_format (str): html, png or pdf plots to be generated
            output_path(str): path where the html plots or report to be stored
            report (bool): if true generates a report
            report_format (:obj: `ReportParam`): whether the attribute
            distribution should be on same page or continued on different pages
            file_name(str): file name of report pdf
            target(str): Name of target variable in dataset
        """

        if report:
            output_format = utils.PNG

        for attr in self.attribute_info:
            self.render_univariate(attr_name=attr.name,
                                   output_format=output_format, target=target)
        self.render_multivariate(output_format=output_format)

        self.scatter(output_format=output_format, target=target)

        if report:
            CombinedReport(dataset=self.dataset).create_report(
                output_path=output_path, file_name=file_name,
                report_format=report_format)

    def univariate_plot(self, attr_name=None, attr_type=None, plot_type=None,
                        output_format=utils.HTML,
                        others_percent_threshold=None,
                        bar_plot_tail_threshold=None):
        """
        Helper function to generate univariate plots
        Args:
            attr_name(:str): optional attribute name to plot specific
                attribute plots
            attr_type(:str): optional attribute type to generate all plots
                for specific attribute type
            plot_type(:str): Plot type to be generated for corresponding
                attribute name or type
            output_format (str): html, png or pdf plots to be generated
            others_percent_threshold(int): Optional threshold to club small
                categories in pie chart
            bar_plot_tail_threshold(int): Optional threshold to club tail
                in bar chart
        """
        possible_plots = {
            DataType.NUMERIC.value: [PlotType.QUARTILE.value,
                                     PlotType.BAR.value],
            DataType.NOMINAL.value: [PlotType.PIE.value, PlotType.BAR.value],
            DataType.ORDINAL.value: [PlotType.PIE.value, PlotType.BAR.value],
            DataType.DATE.value: [PlotType.PIE.value, PlotType.BAR.value]
        }
        attr_type_plot_mapping = {
            DataType.NUMERIC.value: [PlotType.QUARTILE.value,
                                     PlotType.BAR.value],
            DataType.NOMINAL.value: [PlotType.PIE.value],
            DataType.ORDINAL.value: [PlotType.PIE.value],
            DataType.DATE.value: [PlotType.PIE.value]
        }
        is_plotted = False
        if plot_type is not None and attr_type is not None:
            if plot_type in possible_plots[attr_type]:
                attr_type_plot_mapping[attr_type] = [plot_type]
            else:
                self.logger.info(
                    "{} plot not possible for {} type. Hence plotting "
                    "default.".format(plot_type, attr_type))
        if plot_type is not None and attr_name is not None:
            attr_list = list(filter(lambda attr: attr.name == attr_name,
                                    self.attribute_info))
            if not attr_list:
                self.logger.error(
                    "{} attribute doesn't exist".format(attr_name))
                return
            attr = attr_list[0]
            if plot_type in possible_plots[attr.type]:
                attr_type_plot_mapping[attr.type] = [plot_type]
            else:
                self.logger.info("{} plot not possible for {} attribute. "
                                 "Hence plotting default.".format(plot_type,
                                                                  attr_name))
        for attr in self.attribute_info:

            if (attr.name != attr_name and attr_name is not None) \
                    or (self.dataset.data[attr.name].isnull().all()):
                continue

            if attr.type == DataType.NUMERIC.value and \
                    PlotType.QUARTILE.value in \
                    attr_type_plot_mapping[attr.type]:
                field = "quartiles"
                is_plotted = self.plot(field, attr, PlotType.QUARTILE.value,
                                       output_format)
            if attr.type == DataType.NUMERIC.value and \
                    PlotType.BAR.value in attr_type_plot_mapping[attr.type]:
                field = "pdf"
                is_plotted = self.plot(field, attr, PlotType.BAR.value,
                                       output_format,
                                       bar_plot_tail_threshold=bar_plot_tail_threshold)

            if (
                    attr.type == DataType.NOMINAL.value or attr.type ==
                    DataType.ORDINAL.value) \
                    and PlotType.PIE.value in attr_type_plot_mapping[attr.type]:
                field = "freq_count"
                is_plotted = self.plot(field, attr, PlotType.PIE.value,
                                       output_format,
                                       others_percent_threshold=others_percent_threshold)

            if (
                    attr.type == DataType.NOMINAL.value or attr.type ==
                    DataType.ORDINAL.value) \
                    and PlotType.BAR.value in attr_type_plot_mapping[attr.type]:
                field = "freq_count"
                is_plotted = self.plot(field, attr, PlotType.BAR.value,
                                       output_format,
                                       bar_plot_tail_threshold=bar_plot_tail_threshold)

            if attr.type == DataType.DATE.value and \
                    PlotType.PIE.value in attr_type_plot_mapping[attr.type]:
                plot_key = ["day_count", "month_count", "year_count"]
                for field in plot_key:
                    is_plotted = self.plot(field, attr, PlotType.PIE.value,
                                           output_format,
                                           others_percent_threshold=others_percent_threshold)

            if attr.type == DataType.DATE.value and \
                    PlotType.BAR.value in attr_type_plot_mapping[attr.type]:
                plot_key = ["day_count", "month_count", "year_count"]
                for field in plot_key:
                    is_plotted = self.plot(field, attr, PlotType.BAR.value,
                                           output_format,
                                           bar_plot_tail_threshold=bar_plot_tail_threshold)

            if is_plotted is False:
                self.logger.error("Unable to plot for Attribute : ({},{}) "
                                  "with provided plot types parameter: {}"
                                  .format(attr.name, attr.type, plot_type))

    def scatter(self, attribute_x=None, attribute_y=None,
                output_format=utils.HTML, file_name=None,
                output_path=REPORT_OUTPUT_PATH, report=False, target=False):
        """
        Renders scatter plots for the numeric attributes
        Args:
            attribute_x(str): x axis attribute
            attribute_y(str): y axis attribute
            output_format (str): html, png or pdf plots to be generated
            output_path(str): path where the html/pdf/png plots to be stored
            file_name(str): File name of the plot to be stored
            report (bool): if true generates a report
            target(str): Name of target variable in dataset
        """
        pass

    def process_scatter_input(self, attribute_x=None, attribute_y=None,
                              output_format=utils.HTML, report=False):
        if report:
            output_format = utils.PNG
        numeric_attr = self.dataset.info.metrics["numeric_attributes"]
        if attribute_x is not None and attribute_x not in numeric_attr:
            self.logger.info("{} not in numeric attribute".format(attribute_x))
            return False
        if attribute_y is not None and attribute_y not in numeric_attr:
            self.logger.info("{} not in numeric attribute".format(attribute_y))
            return False
        return output_format, numeric_attr
